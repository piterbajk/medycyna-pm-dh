import pandas
import numpy as np
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2

data = pandas.read_csv('normalized.csv')
X = data.drop('diagnosis', axis = 1)
y = data.diagnosis

def print_ranking_with_labels(ranking):
    first_n = 15
    pairs = zip(data.columns, ranking)
    pairs = sorted(pairs, key=lambda x: x[1])
    pairs = list(reversed(pairs))
    for pair in pairs[:first_n]:
        print ('{:<25} {:>5.4f}'.format(pair[0], pair[1]))
        
def get_best_features_labels(ranking, best_n):
    pairs = zip(data.columns, ranking)
    pairs = sorted(pairs, key=lambda x: x[1])
    pairs = list(reversed(pairs))
    names = [i[0] for i in pairs]
    return names[:best_n]

from sklearn.ensemble import ExtraTreesClassifier
print('\n\nExtraTreesClassifier ranking')
model = ExtraTreesClassifier(n_estimators=100)
model.fit(X, y)
print_ranking_with_labels(model.feature_importances_)

#print data[names]

from sklearn.neural_network import MLPClassifier
max_iter = 400
mlp50 = MLPClassifier(hidden_layer_sizes=(50), max_iter=max_iter)
mlp100 = MLPClassifier(hidden_layer_sizes=(100), max_iter=max_iter)
mlp200 = MLPClassifier(hidden_layer_sizes=(200), max_iter=max_iter)
# print(mlp.fit(columns,y[:2]))

from mlxtend.evaluate import paired_ttest_5x2cv
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score 
import statistics as stat

accuracy50 = []
accuracy100 = []
accuracy200 = []

accuracy_f1_50 = []
accuracy_f1_100 = []
accuracy_f1_200 = []

for i in range(1, len(X.columns)+1):
    names = get_best_features_labels(model.feature_importances_, i)

    score50 = []
    score100 = []
    score200 = []
       
    f1_50 = []
    f1_100 = []
    f1_200 = []

    test_50_100 = []
    test_50_200 = []
    test_100_200 = []

    for j in range(5):
        X_train, X_test, y_train, y_test = \
            train_test_split(X[names], y, test_size=0.5, random_state=1)
                
        #SCORES CV2 mlp50, mlp100, mlp200   
        score50a=mlp50.fit(X_train, y_train).score(X_test, y_test)
        score50b=mlp50.fit(X_test, y_test).score(X_train, y_train) #test <--> train
        score50.extend([score50a, score50b])

        score100a=mlp100.fit(X_train, y_train).score(X_test, y_test)
        score100b=mlp100.fit(X_test, y_test).score(X_train, y_train) 
        score100.extend([score100a, score100b])

        score200a=mlp200.fit(X_train, y_train).score(X_test, y_test)
        score200b=mlp200.fit(X_test, y_test).score(X_train, y_train) #test <--> train
        score200.extend([score200a, score200b])

        #F1 SCORES mlp50, mlp100, mlp200
        f1_50a=f1_score(y_test, mlp50.predict(X_test))
        f1_50b=f1_score(y_train, mlp50.predict(X_train)) #test <--> train
        f1_50.extend([f1_50a, f1_50b])
        
        f1_100a=f1_score(y_test, mlp100.predict(X_test))
        f1_100b=f1_score(y_train, mlp100.predict(X_train)) #test <--> train
        f1_100.extend([f1_100a, f1_100b])
        
        f1_200a=f1_score(y_test, mlp200.predict(X_test))
        f1_200b=f1_score(y_train, mlp200.predict(X_train)) #test <--> train
        f1_200.extend([f1_200a, f1_200b])


    # append scores to compare classifiers 
    from scipy import stats
    t50_100 = stats.ttest_ind(score50,score100)
    t50_200 = stats.ttest_ind(score50,score200)
    t100_200 = stats.ttest_ind(score100,score200)

    print(t50_100)
    print(t50_200)
    print(t100_200)

    test_50_100.append(t50_100)
    test_50_200.append(t50_200)
    test_100_200.append(t100_200)

    scores=np.array([stat.mean(score50),stat.mean(score100),stat.mean(score200)])
    f1=np.array([stat.mean(f1_50),stat.mean(f1_100),stat.mean(f1_200)])
    
    print('\nNumber of features used: {}'.format(i))
    print('MLP (50 neurons) accuracy:  %.2f%%' % (100*scores[0]))
    print('MLP (100 neurons) accuracy: %.2f%%' % (100*scores[1]))
    print('MLP (200 neurons) accuracy: %.2f%%' % (100*scores[2]))

    print('\nF1 score: [0 ... 1]')
    print('MLP (50 neurons):  %.4f' % f1[0])
    print('MLP (100 neurons): %.4f' % f1[1])
    print('MLP (200 neurons): %.4f' % f1[2])
    
    accuracy50.append(100*scores[0])
    accuracy100.append(100*scores[1])
    accuracy200.append(100*scores[2])

    accuracy_f1_50.append(f1[0])
    accuracy_f1_100.append(f1[1])
    accuracy_f1_200.append(f1[2])
    print('\n\n')


import matplotlib.pyplot as plt
epochs = range(1, len(X.columns)+1)
plt.figure(1)
plt.plot(epochs, accuracy50, label='50 neurons')
plt.plot(epochs, accuracy100, label='100 neurons')
plt.plot(epochs, accuracy200, label='200 neurons')
plt.xlabel('Number of features')
plt.ylabel('Accuracy')
plt.legend()

plt.figure(2)
plt.plot(epochs, accuracy_f1_50, label='50 neurons')
plt.plot(epochs, accuracy_f1_100, label='100 neurons')
plt.plot(epochs, accuracy_f1_200, label='200 neurons')
plt.xlabel('Number of features')
plt.ylabel('F1 Scores')
plt.legend()

plt.figure(3)
plt.plot(epochs, test_50_100, label='mlp50 vs mlp100')
plt.plot(epochs, test_50_200, label='mlp50 vs mlp200')
plt.plot(epochs, test_100_200, label='mlp100 vs mlp200')
plt.xlabel('Number of features')
plt.ylabel('Classfiers accuracy comparison')
plt.legend()

plt.show()
