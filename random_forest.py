import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import cross_val_score

# data = pd.read_csv('thyroid_nulls_bindiag.csv')
data = pd.read_csv('out.csv')
X = data.drop('diagnosis', axis = 1)
y = data.diagnosis

print X

clf = RandomForestClassifier(n_estimators = 100, max_depth = 4)

scores = []
num_features = len(X.columns)
for i in range(num_features):
    col = X.columns[i]
    print cross_val_score(clf, X[col].values.reshape(-1,1), y, cv=5)
    #score = np.mean(cross_val_score(clf, X[col].values.reshape(-1,1), y, cv=2))
    #scores.append((int(score*100), col))

for elem in sorted(scores, reverse = True):
    print elem
