import pandas as pd 
import numpy as np 
from sklearn.preprocessing import StandardScaler

def scale(data):
    scaler = StandardScaler()
    scaler.fit(data)
    return scaler.transform(data)

def normalize(data):
    results = []
    for c in data.columns:
        ctype = data[c].dtype
        
        if ctype == 'float64':
            out[c] = scale(data[c].values.reshape(-1,1))
            out[c] = out[c].round(5)
        else:
            out[c] = data[c]

data = pd.read_csv('out.csv')
out = pd.DataFrame()
normalize(data)
out['diagnosis'] = data.diagnosis
out.to_csv('normalized.csv')
